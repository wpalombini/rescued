import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormsComponent } from 'src/app/guards/forms-component';
import { Observable, Subscription, of } from 'rxjs';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent extends FormsComponent implements OnInit, OnDestroy {
    public fullName: string;

    private subscription: Subscription = new Subscription();

    constructor(route: ActivatedRoute, private authenticationService: AuthenticationService) {
        super(route);
    }

    public ngOnInit(): void {
        this.subscription.add(
            this.authenticationService.isAuthenticated().subscribe((isAuthenticated: boolean): void => {
                this.fullName = isAuthenticated ? this.authenticationService.user.fullName : null;
            })
        );
    }

    public ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    public canDeactivate: () => Observable<boolean> = () => {
        return of(true);
    };
}
