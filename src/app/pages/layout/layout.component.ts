import { Component, ChangeDetectorRef, OnDestroy, ViewChild } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { slideInAnimation } from '../../animations/animations';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { AccountDataService } from 'src/app/services/account-data.service';
import { tap, switchMap, map, take } from 'rxjs/operators';
import { Observable, of, Subscription } from 'rxjs';
import { AccountModel } from 'src/app/models/account.model';
import { SanctuaryModel } from 'src/app/models/sanctuary.model';
import { SanctuaryDataService } from 'src/app/services/sanctuary-data.service';

@Component({
    animations: [slideInAnimation],
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnDestroy {
    public mobileQuery: MediaQueryList;

    @ViewChild('snav')
    public sideNavPanel: MatSidenav;

    public get accountHasSanctuary(): boolean {
        return this.accountModel && this.accountModel.sanctuaries && this.accountModel.sanctuaries.length > 0;
    }
    public get accountHasNoSanctuary(): boolean {
        return this.accountModel && (!this.accountModel.sanctuaries || this.accountModel.sanctuaries.length < 1);
    }
    public accountModel: AccountModel;
    public sanctuaryModel: SanctuaryModel;
    public isAuthenticated: boolean = false;
    public get accountFullName(): string | null {
        return this.authenticationService.user?.fullName;
    }

    private mobileQueryListener: () => void;
    private subscription: Subscription = new Subscription();

    constructor(
        changeDetectorRef: ChangeDetectorRef,
        media: MediaMatcher,
        private accountDataService: AccountDataService,
        private authenticationService: AuthenticationService,
        private sanctuaryService: SanctuaryDataService,
        private router: Router
    ) {
        this.mobileQuery = media.matchMedia('(max-width: 1330px)');
        this.mobileQueryListener = (): void => changeDetectorRef.detectChanges();
        // tslint:disable-next-line:deprecation
        this.mobileQuery.addListener(this.mobileQueryListener);

        this.subscription.add(
            this.authenticationService
                .isAuthenticated()
                .pipe(
                    map((result: boolean): string | null => {
                        this.isAuthenticated = result;
                        return this.isAuthenticated ? this.authenticationService.user.id : null;
                    }),
                    switchMap((accountId: string | null): Observable<AccountModel> => {
                        return this.isAuthenticated
                            ? this.accountDataService.getSanctuaryIdsForAccount(accountId).pipe(take(1))
                            : of(null);
                    }),
                    tap((accountModel: AccountModel): void => {
                        this.accountModel = accountModel;
                    }),
                    switchMap((accountModel: AccountModel): Observable<SanctuaryModel> => {
                        return accountModel !== null
                            // dont unsubscribe from this as we need to get updates when someone else updates the sanctuary details
                            ? this.sanctuaryService.getSanctuaryById(accountModel.sanctuaries[0])
                            : of(null);
                    }),
                    tap((sanctuaryModel: SanctuaryModel): void => {
                        this.sanctuaryModel = sanctuaryModel;
                    })
                )
                .subscribe()
        );
    }

    ngOnDestroy(): void {
        // tslint:disable-next-line:deprecation
        this.mobileQuery.removeListener(this.mobileQueryListener);
        this.subscription.unsubscribe();
    }

    public onLinkClicked(): void {
        if (this.mobileQuery.matches) {
            this.sideNavPanel.close();
        }
    }

    public onLogout(): void {
        this.onLinkClicked();

        // if we navigate away before fully logging out, we hit routing guards still as authenticated.
        this.subscription.add(this.authenticationService.logout().subscribe((): void => {
            this.router.navigate(['/home']);
        }));
    }
}
