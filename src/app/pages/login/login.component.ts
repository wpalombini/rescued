import { Component, OnDestroy } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ActivatedRoute } from '@angular/router';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnDestroy {
    public signup: boolean;

    private linkSanctuaryId: string;
    private subscription: Subscription = new Subscription();

    constructor(
        private authenticationService: AuthenticationService,
        route: ActivatedRoute,
        iconRegistry: MatIconRegistry,
        sanitizer: DomSanitizer
    ) {
        this.signup = route.snapshot.data['signup'] as boolean;
        if (this.signup) {
            try {
                this.linkSanctuaryId = route.snapshot.queryParams['ls'];
            } catch (_err) { }
        }

        iconRegistry.addSvgIcon(
            'google-logo',
            sanitizer.bypassSecurityTrustResourceUrl('assets/img/logos/google-logo.svg')
        );
    }

    public ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    public loginWithGoogle(): void {
        this.subscription.add(
            this.authenticationService
                .loginWithGoogle(this.signup, this.linkSanctuaryId)
                .subscribe()
        );
    }
}
