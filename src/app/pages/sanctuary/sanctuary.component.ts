import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { FormsComponent } from 'src/app/guards/forms-component';
import { Observable, of, Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, AbstractControl, NgForm } from '@angular/forms';
import { DialogService } from 'src/app/services/dialog.service';
import { SanctuaryDataService } from 'src/app/services/sanctuary-data.service';
import { SanctuaryModel } from 'src/app/models/sanctuary.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ResultMessageService } from 'src/app/services/result-message.service';

@Component({
    selector: 'app-sanctuary',
    templateUrl: './sanctuary.component.html',
    styleUrls: ['./sanctuary.component.scss'],
})
export class SanctuaryComponent extends FormsComponent implements OnInit, OnDestroy {
    public form: FormGroup;

    private sanctuaryId: string | undefined;
    private subscription: Subscription = new Subscription();

    @ViewChild('formDirective')
    public formDirective: NgForm;

    public get email(): AbstractControl {
        return this.form.get('sanctuaryEmail');
    }

    public get name(): AbstractControl {
        return this.form.get('sanctuaryName');
    }

    constructor(
        route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private dialogService: DialogService,
        private sanctuaryService: SanctuaryDataService,
        private authenticationService: AuthenticationService,
        private resultMessageService: ResultMessageService
    ) {
        super(route);
        this.sanctuaryId =
            route.snapshot.params.sanctuaryId !== 'create' ? route.snapshot.params.sanctuaryId : undefined;
        this.initializeForm();
    }

    public ngOnInit(): void {
        if (this.sanctuaryId) {
            this.subscription.add(
                this.sanctuaryService
                    .getSanctuaryById(this.sanctuaryId)
                    .subscribe((sanctuary: SanctuaryModel): void => {
                        if (sanctuary) {
                            this.form.patchValue({
                                sanctuaryName: sanctuary.name,
                                sanctuaryEmail: sanctuary.emailAddress,
                            });
                        }
                    })
            );
        }
    }

    public ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    public canDeactivate: () => Observable<boolean> = () => {
        if (this.form.dirty) {
            return this.dialogService.getConfirmationDialogResult();
        } else {
            return of(true);
        }
    };

    public clear(): void {
        this.formDirective.resetForm();
    }

    public save(): void {
        // mark all controls as touched in order to trigger validation on required controls
        this.form.markAllAsTouched();

        // save if form has been modified and is valid
        if (this.form.dirty && this.form.valid) {
            const sanctuary: SanctuaryModel = new SanctuaryModel();
            sanctuary.emailAddress = this.email.value;
            sanctuary.name = this.name.value;
            sanctuary.id = this.sanctuaryId;

            if (this.sanctuaryId) {
                // update
                this.subscription.add(
                    this.sanctuaryService
                        .updateSanctuary(sanctuary, this.authenticationService.user)
                        .subscribe((success: boolean): void => {
                            if (success) {
                                this.form.markAsPristine();

                                this.resultMessageService.sendTextSuccessNotification();
                            }
                        })
                );
            } else {
                // create
                this.subscription.add(
                    this.sanctuaryService
                        .createSanctuary(sanctuary, this.authenticationService.user)
                        .subscribe((_success: boolean): void => {})
                );
            }
        }
    }

    private initializeForm(): void {
        this.form = this.formBuilder.group({
            sanctuaryName: ['', Validators.required],
            sanctuaryEmail: ['', [Validators.required, Validators.email]],
        });
    }
}
