import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SanctuaryComponent } from './sanctuary.component';

describe('SanctuaryComponent', (): void => {
  let component: SanctuaryComponent;
  let fixture: ComponentFixture<SanctuaryComponent>;

  beforeEach(async((): any => {
    TestBed.configureTestingModule({
      declarations: [ SanctuaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach((): void => {
    fixture = TestBed.createComponent(SanctuaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', (): void => {
    expect(component).toBeTruthy();
  });
});
