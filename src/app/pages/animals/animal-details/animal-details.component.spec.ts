import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimalDetailsComponent } from './animal-details.component';

describe('AnimalDetailsComponent', (): void => {
    let component: AnimalDetailsComponent;
    let fixture: ComponentFixture<AnimalDetailsComponent>;

    beforeEach(async((): void => {
        TestBed.configureTestingModule({
            declarations: [AnimalDetailsComponent],
        }).compileComponents();
    }));

    beforeEach((): void => {
        fixture = TestBed.createComponent(AnimalDetailsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', (): void => {
        expect(component).toBeTruthy();
    });
});
