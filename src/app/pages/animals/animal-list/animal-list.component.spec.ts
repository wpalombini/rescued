import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimalListComponent } from './animal-list.component';

describe('AnimalListComponent', (): void => {
    let component: AnimalListComponent;
    let fixture: ComponentFixture<AnimalListComponent>;

    beforeEach(async((): void => {
        TestBed.configureTestingModule({
            declarations: [AnimalListComponent],
        }).compileComponents();
    }));

    beforeEach((): void => {
        fixture = TestBed.createComponent(AnimalListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', (): void => {
        expect(component).toBeTruthy();
    });
});
