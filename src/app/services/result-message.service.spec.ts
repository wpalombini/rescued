import { TestBed } from '@angular/core/testing';

import { ResultMessageService } from './result-message.service';

describe('ResultMessageService', (): void => {
    let service: ResultMessageService;

    beforeEach((): void => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(ResultMessageService);
    });

    it('should be created', (): void => {
        expect(service).toBeTruthy();
    });
});
