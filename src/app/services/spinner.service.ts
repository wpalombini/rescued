import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class SpinnerService {
    public displaySpinner: boolean = false;

    constructor() {}

    public show(): void {
        this.displaySpinner = true;
    }

    public hide(): void {
        this.displaySpinner = false;
    }
}
