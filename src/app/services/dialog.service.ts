import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../components/confirmation-dialog/confirmation-dialog.component';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class DialogService {
    private dialogWidth: string = '500px';

    constructor(private dialog: MatDialog) {}

    public openConfirmationDialog(): MatDialogRef<ConfirmationDialogComponent, any> {
        const dialogRef: MatDialogRef<ConfirmationDialogComponent, any> = this.dialog.open(
            ConfirmationDialogComponent,
            {
                width: this.dialogWidth,
                data: { name: 'name data' },
            }
        );

        return dialogRef;
    }

    public getConfirmationDialogResult(): Observable<boolean> {
        return this.openConfirmationDialog()
            .afterClosed()
            .pipe(
                take(1),
                map((result: any): boolean => {
                    return result ?? false;
                })
            );
    }
}
