import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Observable, of, from } from 'rxjs';
import { SanctuaryModel } from '../models/sanctuary.model';
import { SpinnerService } from './spinner.service';
import { tap, catchError } from 'rxjs/operators';
import { AccountDataService } from './account-data.service';
import { AccountModel } from '../models/account.model';
import { User } from '../models/user.model';

@Injectable({
    providedIn: 'root',
})
export class SanctuaryDataService {
    public static readonly sanctuariesCollection: string = 'sanctuaries';

    constructor(
        private firestore: AngularFirestore,
        private spinnerService: SpinnerService) { }

    public createSanctuary(_sanctuary: SanctuaryModel, _user: User): Observable<boolean> {

        return of(true);
    }

    public getSanctuaryById(id: string): Observable<SanctuaryModel> {
        if (!id) {
            return of(null);
        }
        this.spinnerService.show();

        return this.firestore
            .collection(SanctuaryDataService.sanctuariesCollection)
            .doc<SanctuaryModel>(id)
            .valueChanges()
            .pipe(
                tap((): void => this.spinnerService.hide()),
                catchError((_err: any): Observable<any> => {
                    this.spinnerService.hide();
                    return of(null);
                })
            );
    }

    public updateSanctuary(sanctuary: SanctuaryModel, user: User): Observable<boolean> {
        // for some reason null or undefined doc Ids are not caught.
        // We have to check it beforehand
        if (!sanctuary.id || !user?.id) {
            // display error message
            return of(false);
        }

        this.spinnerService.show();

        const modifiedByUser: User = user;
        sanctuary.modifiedBy = Object.assign({}, modifiedByUser);
        sanctuary.modifiedAt = new Date().toString();

        const sanctuaryDocRef: DocumentReference = this.firestore
            .collection(SanctuaryDataService.sanctuariesCollection)
            .doc<SanctuaryModel>(sanctuary.id)
            .ref;

        const accountDocRef: DocumentReference = this.firestore
            .collection(AccountDataService.accountCollection)
            .doc<AccountModel>(user.id)
            .ref;

        return from(
            this.firestore.firestore.runTransaction<boolean>((transaction: firebase.firestore.Transaction): Promise<boolean> => {
                return new Promise((resolve: (value?: boolean | PromiseLike<boolean>) => any): void => {
                    transaction.update(sanctuaryDocRef, Object.assign({}, sanctuary));

                    // updating the account document is not necessary as the sanctuary id is not changing.
                    // this is just an experiment with transactions
                    transaction.update(accountDocRef, { sanctuaries: [sanctuary.id] });

                    resolve(true);
                });
            }))
            .pipe(
                catchError((_err: any): Observable<boolean> => {
                    // display error message
                    return of(false);
                }),
                tap((): void => this.spinnerService.hide())
            );
    }
}
