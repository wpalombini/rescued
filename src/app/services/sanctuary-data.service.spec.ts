import { TestBed } from '@angular/core/testing';

import { SanctuaryDataService } from './sanctuary-data.service';

describe('SanctuaryDataService', (): void => {
    let service: SanctuaryDataService;

    beforeEach((): void => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(SanctuaryDataService);
    });

    it('should be created', (): void => {
        expect(service).toBeTruthy();
    });
});
