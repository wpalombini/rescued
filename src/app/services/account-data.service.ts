import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, of, from } from 'rxjs';
import { AccountModel } from '../models/account.model';
import { SpinnerService } from './spinner.service';
import { tap, catchError, map } from 'rxjs/operators';
import { User } from '../models/user.model';

@Injectable({
    providedIn: 'root',
})
export class AccountDataService {
    public static readonly accountCollection: string = 'accounts';

    constructor(
        private firestore: AngularFirestore,
        private spinnerService: SpinnerService,
    ) { }

    public createAccount(user: User, linkSanctuaryId: string | null = null): Observable<boolean> {
        // create account if not exists
        // if linkSanctuaryId, add the sanctuary to the account.Sanctuaries array

        this.spinnerService.show();

        const account: AccountModel = new AccountModel();
        account.createdAt = new Date().toString();
        account.createdBy = user;
        account.id = user.id;
        account.modifiedAt = account.createdAt;
        account.modifiedBy = user;

        return from(
            this.firestore
                .collection(AccountDataService.accountCollection)
                .doc<AccountModel>(user.id)
                .set({
                    id: account.id,
                    createdAt: account.createdAt,
                    createdBy: Object.assign({}, account.createdBy),
                    modifiedAt: account.modifiedAt,
                    modifiedBy: Object.assign({}, account.modifiedBy),
                    sanctuaries: linkSanctuaryId ? [linkSanctuaryId] : []
                }))
            .pipe(
                catchError((_err: any): Observable<boolean> => {
                    this.spinnerService.hide();
                    return of(false);
                }),
                map((): boolean => {
                    this.spinnerService.hide();
                    return true;
                }),
            );
    }

    public getSanctuaryIdsForAccount(accountId: string): Observable<AccountModel> {
        this.spinnerService.show();

        return this.firestore
            .collection(AccountDataService.accountCollection)
            .doc<AccountModel>(accountId)
            .valueChanges()
            .pipe(
                tap((): void => this.spinnerService.hide()),
                catchError(
                    (_err: any): Observable<any> => {
                        this.spinnerService.hide();
                        return of(null);
                    }
                )
            );
    }
}
