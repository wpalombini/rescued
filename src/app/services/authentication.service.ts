import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Observable, from, of } from 'rxjs';
import { map, tap, take, switchMap } from 'rxjs/operators';
import { SpinnerService } from './spinner.service';
import { StorageService } from './storage.service';
import { AccountDataService } from './account-data.service';
import { User } from '../models/user.model';

@Injectable({
    providedIn: 'root',
})
export class AuthenticationService {
    public user: User;

    constructor(
        private afAuth: AngularFireAuth,
        private spinnerService: SpinnerService,
        private storageService: StorageService,
        private accountService: AccountDataService
    ) { }

    public isAuthenticated(): Observable<boolean> {
        this.spinnerService.show();
        return this.afAuth.user.pipe(
            tap((_user: firebase.User): void => {
                this.spinnerService.hide();
            }),
            map((user: firebase.User): boolean => {
                this.setUser(user);
                return user != null;
            })
        );
    }

    public finaliseSignUp(): void {
        const linkSanctuaryId: string = this.storageService.getFromSession(StorageService.linkSanctuary);
        this.storageService.deleteFromSession(StorageService.linkSanctuary);

        this.isAuthenticated()
            .pipe(
                take(1),
                switchMap(
                    (isAuthenticated: boolean): Observable<boolean> => {
                        if (isAuthenticated) {
                            return this.accountService.createAccount(this.user, linkSanctuaryId).pipe(take(1));
                        } else {
                            return of(false);
                        }
                    }
                )
            )
            .subscribe();
    }

    public loginWithGoogle(isSigningUp: boolean, linkSanctuaryId: string = null): Observable<boolean> {
        const provider: auth.GoogleAuthProvider = new auth.GoogleAuthProvider();

        if (isSigningUp) {
            this.storageService.saveToSession(StorageService.isSigningUp, isSigningUp);

            if (linkSanctuaryId) {
                this.storageService.saveToSession(StorageService.linkSanctuary, linkSanctuaryId);
            }
        }

        return from(
            this.afAuth
                .signInWithRedirect(provider)
                .then((): boolean => true)
                .catch((_err: any): boolean => false)
        );
    }

    public logout(): Observable<void> {
        return from(this.afAuth.signOut());
    }

    private setUser(user: firebase.User): void {
        if (user != null) {
            this.user = new User(user.uid, user.email, user.displayName);
        } else {
            this.user = null;
        }
    }
}
