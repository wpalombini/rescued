import { TestBed } from '@angular/core/testing';

import { AccountDataService } from './account-data.service';

describe('AccountDataService', (): void => {
    let service: AccountDataService;

    beforeEach((): void => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(AccountDataService);
    });

    it('should be created', (): void => {
        expect(service).toBeTruthy();
    });
});
