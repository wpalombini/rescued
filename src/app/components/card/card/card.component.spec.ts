import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardComponent } from './card.component';

describe('CardComponent', (): void => {
    let component: CardComponent;
    let fixture: ComponentFixture<CardComponent>;

    beforeEach(async((): void => {
        TestBed.configureTestingModule({
            declarations: [CardComponent],
        }).compileComponents();
    }));

    beforeEach((): void => {
        fixture = TestBed.createComponent(CardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', (): void => {
        expect(component).toBeTruthy();
    });
});
