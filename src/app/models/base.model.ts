import { User } from './user.model';

export abstract class BaseModel {
    public id: string;
    public createdBy: User;
    public createdAt: string;
    public modifiedBy: User;
    public modifiedAt: string;
}
