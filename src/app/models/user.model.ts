export class User {
    constructor(id: string, email: string, name: string) {
        this.id = id;
        this.emailAddress = email;
        this.fullName = name;
    }

    public readonly id: string;
    public readonly emailAddress: string;
    public readonly fullName: string;
}
