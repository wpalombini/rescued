import { BaseModel } from './base.model';

export class AccountModel extends BaseModel {
    sanctuaries: Array<string> = new Array<string>();
}
