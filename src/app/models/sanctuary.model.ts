import { BaseModel } from './base.model';

export class SanctuaryModel extends BaseModel {
    public emailAddress: string;
    public name: string;
}
