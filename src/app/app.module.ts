import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { LayoutComponent } from './pages/layout/layout.component';
import { AboutComponent } from './pages/about/about.component';
import { AccountComponent } from './pages/account/account.component';
import { SanctuaryComponent } from './pages/sanctuary/sanctuary.component';
import { HomeComponent } from './pages/home/home.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { DialogService } from './services/dialog.service';
import { AuthenticationService } from './services/authentication.service';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { LoginComponent } from './pages/login/login.component';
import { MaterialModule } from 'src/material/material.module';
import { AnimalListComponent } from './pages/animals/animal-list/animal-list.component';
import { AnimalDetailsComponent } from './pages/animals/animal-details/animal-details.component';
import { AccountDataService } from './services/account-data.service';
import { ReactiveFormsModule } from '@angular/forms';
import { CardComponent } from './components/card/card/card.component';

@NgModule({
    declarations: [
        AboutComponent,
        AccountComponent,
        AppComponent,
        ConfirmationDialogComponent,
        HomeComponent,
        LayoutComponent,
        LoginComponent,
        SettingsComponent,
        SpinnerComponent,
        SanctuaryComponent,
        AnimalListComponent,
        AnimalDetailsComponent,
        CardComponent,
    ],
    entryComponents: [ConfirmationDialogComponent],
    imports: [
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientModule,
        MaterialModule,
    ],
    providers: [AccountDataService, AuthenticationService, DialogService],
    bootstrap: [AppComponent],
})
export class AppModule {}
