import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';

import { AboutComponent } from './pages/about/about.component';
import { AccountComponent } from './pages/account/account.component';
import { SanctuaryComponent } from './pages/sanctuary/sanctuary.component';
import { HomeComponent } from './pages/home/home.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { CanComponentDeactivateGuard } from './guards/can-component-deactivate.guard';
import { AuthenticationGuard } from './guards/authentication.guard';
import { LoginComponent } from './pages/login/login.component';
import { AuthenticatedGuard } from './guards/authenticated.guard';
import { AnimalListComponent } from './pages/animals/animal-list/animal-list.component';
import { AnimalDetailsComponent } from './pages/animals/animal-details/animal-details.component';

const routes: Routes = [
    { path: 'login', component: LoginComponent, canActivate: [AuthenticatedGuard] },
    { path: 'signup', component: LoginComponent, canActivate: [AuthenticatedGuard], data: { signup: true } },
    { path: 'sanctuary/:sanctuaryId/animal/:animalId', component: AnimalDetailsComponent, canActivate: [AuthenticationGuard] },
    { path: 'sanctuary/:sanctuaryId/animal', component: AnimalListComponent, canActivate: [AuthenticationGuard] },
    {
        path: 'sanctuary/:sanctuaryId',
        component: SanctuaryComponent,
        canActivate: [AuthenticationGuard],
        canDeactivate: [CanComponentDeactivateGuard],
    },
    { path: 'account', component: AccountComponent, canActivate: [AuthenticationGuard] },
    { path: 'about', component: AboutComponent },
    {
        path: 'settings',
        component: SettingsComponent,
        canActivate: [AuthenticationGuard],
        canDeactivate: [CanComponentDeactivateGuard],
    },
    { path: '', component: HomeComponent },
    { path: '**', redirectTo: '/', pathMatch: 'full' },
];

const options: ExtraOptions = {
    // enableTracing: !environment.production,
    onSameUrlNavigation: 'reload',
    scrollPositionRestoration: 'top',
};
@NgModule({
    imports: [RouterModule.forRoot(routes, options)],
    providers: [AuthenticationGuard, CanComponentDeactivateGuard],
    exports: [RouterModule],
})
export class AppRoutingModule { }
