import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from './services/storage.service';
import { AuthenticationService } from './services/authentication.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    constructor(router: Router, storageService: StorageService, authenticationService: AuthenticationService) {
        const isSigningUp: boolean = storageService.getFromSession(StorageService.isSigningUp);
        const returnUrl: string = storageService.getFromSession(StorageService.returnUrl);

        if (isSigningUp) {
            storageService.deleteFromSession(StorageService.isSigningUp);
            authenticationService.finaliseSignUp();
        } else if (returnUrl) {
            storageService.deleteFromSession(StorageService.returnUrl);
            router.navigate([returnUrl]);
        }
    }
}
