import { TestBed } from '@angular/core/testing';

import { AuthenticationGuard } from './authentication.guard';

describe('CanComponentActivateGuard', (): void => {
    let guard: AuthenticationGuard;

    beforeEach((): void => {
        TestBed.configureTestingModule({});
        guard = TestBed.inject(AuthenticationGuard);
    });

    it('should be created', (): void => {
        expect(guard).toBeTruthy();
    });
});
