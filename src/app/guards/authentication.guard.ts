import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';
import { map } from 'rxjs/operators';
import { StorageService } from '../services/storage.service';

@Injectable({
    providedIn: 'root',
})
export class AuthenticationGuard implements CanActivate {
    constructor(
        private authenticationService: AuthenticationService,
        private storageService: StorageService,
        private router: Router
    ) {}

    canActivate(
        _next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.authenticationService.isAuthenticated().pipe(
            map((isAuthenticated: boolean): boolean | UrlTree => {
                if (isAuthenticated) {
                    return true;
                }

                this.storageService.saveToSession(StorageService.returnUrl, state.url);
                return this.router.parseUrl('login');
            })
        );
    }
}
