import { TestBed } from '@angular/core/testing';

import { AuthenticatedGuard } from './authenticated.guard';

describe('AuthenticatedGuard', (): void => {
    let guard: AuthenticatedGuard;

    beforeEach((): void => {
        TestBed.configureTestingModule({});
        guard = TestBed.inject(AuthenticatedGuard);
    });

    it('should be created', (): void => {
        expect(guard).toBeTruthy();
    });
});
