import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { FormsComponent } from './forms-component';
import { AuthenticationService } from '../services/authentication.service';
import { switchMap } from 'rxjs/operators';

@Injectable()
export class CanComponentDeactivateGuard implements CanDeactivate<FormsComponent> {
    constructor(private authenticationService: AuthenticationService) { }

    canDeactivate(
        component: FormsComponent,
        _currentRoute: ActivatedRouteSnapshot,
        _currentState: RouterStateSnapshot,
        _nextState: RouterStateSnapshot
    ): Observable<boolean> | boolean {
        return this.authenticationService.isAuthenticated().pipe(
            switchMap(
                (isAuthenticated: boolean): Observable<boolean> => {
                    if (isAuthenticated) {
                        return component.canDeactivate();
                    } else {
                        return of(true);
                    }
                }
            )
        );
    }
}
