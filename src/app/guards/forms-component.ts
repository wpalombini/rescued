import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

export abstract class FormsComponent {
    public isCreating: boolean;

    constructor(route: ActivatedRoute) {
        this.isCreating = route.snapshot.params.id === 'create' ? true : false;
    }

    public abstract canDeactivate: () => Observable<boolean>;
}
