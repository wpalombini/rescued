export const environment = {
    production: true,
    firebase: {
        apiKey: 'AIzaSyBmZfSN4qZ8uyfUXgTahfEPLLSIIOyX6I4',
        authDomain: 'rescued-e374f.firebaseapp.com',
        databaseURL: 'https://rescued-e374f.firebaseio.com',
        projectId: 'rescued-e374f',
        storageBucket: 'rescued-e374f.appspot.com',
        messagingSenderId: '305619168657',
        appId: '1:305619168657:web:ba2519f7529058b6ea7e30',
        measurementId: 'G-NNQ8BX9BBP',
    },
};
