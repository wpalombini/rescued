// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment: any = {
    production: false,
    firebase: {
        apiKey: 'AIzaSyBmZfSN4qZ8uyfUXgTahfEPLLSIIOyX6I4',
        authDomain: 'rescued-e374f.firebaseapp.com',
        databaseURL: 'https://rescued-e374f.firebaseio.com',
        projectId: 'rescued-e374f',
        storageBucket: 'rescued-e374f.appspot.com',
        messagingSenderId: '305619168657',
        appId: '1:305619168657:web:ba2519f7529058b6ea7e30',
        measurementId: 'G-NNQ8BX9BBP',
    },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
